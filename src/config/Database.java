/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Widyaranti
 */
public class Database {
    private static Connection conn;
    
    public static Connection getConnection(){
        if(conn==null){
            String url = "jdbc:mysql://localhost:3306/kuliah_oop2_project";
            String user="root";
            String password="";
            
            try{
                Driver driver = new Driver();
                DriverManager.registerDriver(driver);
                conn=DriverManager.getConnection(url,user,password);
                System.out.println("Connection success");
            }
            catch(SQLException ex){
                System.out.println("Connection failed");
                System.out.println(ex.getMessage());
            }
        }
        
        return conn;
    }
}
