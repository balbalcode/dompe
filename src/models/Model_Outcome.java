/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Widyaranti
 */
public interface Model_Outcome {
    public String getOutcomeId();
    public String getUserId();
    public String getOutcomeDate();
    public int getOutcomeValue();
    public String getOutcomeCategoryId();
    public String getOutcomeDesc();
    
    public void setOutcomeId(String outcomeId);
    public void setUserId(String userId);
    public void setOutcomeDate(String outcomeDate);
    public void setOutcomeValue(int outcomeValue);
    public void setOutcomeCategoryId(String categoryId);
    public void setOutcomeDesc(String outcomeDesc);
    
}
