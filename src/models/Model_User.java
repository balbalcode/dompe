/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Widyaranti
 */
public interface Model_User {
    public String getUserId();
    public String getUsername();
    public String getPassword();
    public String getEmail();
    public String getPhoneNumber();
    public String getUserNote();
    public String getFullName();
    public Byte[] getImage();
    
    public void setUserId(String userId);
    public void setUsername(String username);
    public void setPassword(String password);
    public void setEmail(String email);
    public void setPhoneNumber(String phoneNumber);
    public void setUserNote(String userNote);
    public void setFullName(String fullName);
    public void setImage(Byte[] image);
    
    public void Login();
}
