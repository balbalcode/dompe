/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Widyaranti
 */
public interface Model_CreditUser {
    public String getCreditUserId();
    public String getUserId();
    public int getCredit();
    public int getLastTransect();
    
    public void setCreditUserId(String creditUserId);
    public void setUserId(String userId);
    public void setCredit(int credit);
    public void setLastTransect(int lastTransect);
}
