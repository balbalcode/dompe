/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Widyaranti
 */
public interface Model_Category {
    public int getCategoryId();
    public int getUserId();
    public String getCategoryName();
    public String geCategoryDesc();
    
    public void setCategoryId(int categoryID);
    public void setUserId(int userId);
    public void setCategoryName(String categoryName);
    public void setCategoryDesc(String categoryDesc);
    
    public void saveCategory();
    public void deleteCategory();
    public void updateCategory();
}
