/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Widyaranti
 */
public interface Model_Income {
    public String getIncomeId();
    public String getUserId();
    public String getIncomeDate();
    public int getIncomeValue();
    public String getIncomeDesc();
    
    public void setIncomeId(String incomeId);
    public void setUserId(String userId);
    public void setIncomeDate(String incomeDate);   //apakah ini nanti pake date format atau sesuatu, 
                                        //gw blm tau
                                        //jadi string dulu y
    public void setIncomeValue(int incomeValue);
    public void setIncomeDesc(String incomeDesc);
}   
