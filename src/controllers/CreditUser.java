/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import config.Database;
import java.sql.Connection;

/**
 *
 * @author Widyaranti
 */
public class CreditUser implements models.Model_CreditUser{
    private String creditUserId;
    private String userId;
    private int credit;
    private int lastTransect;

    private Connection conn=null;
    
    public CreditUser(){
        conn=Database.getConnection();
    }
    
    @Override
    public String getCreditUserId() {
        return creditUserId;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public int getCredit() {
        return credit;
    }

    @Override
    public int getLastTransect() {
        return lastTransect;
    }

    @Override
    public void setCreditUserId(String creditUserId) {
        this.creditUserId=creditUserId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId=userId;
    }

    @Override
    public void setCredit(int credit) {
        this.credit=credit;
    }

    @Override
    public void setLastTransect(int lastTransect) {
        this.lastTransect=lastTransect;
    }
    
    
    
}
