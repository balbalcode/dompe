/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import config.Database;
import java.sql.Connection;

/**
 *
 * @author Widyaranti
 */
public class User implements models.Model_User{
    private String userId;
    private String username;
    private String password;
    private String email;
    private String phoneNumber;
    private String userNote;
    private String fullName;
    private Byte[] image;

    private Connection conn=null;
    
    public User(){
        conn=Database.getConnection();
    }
    
    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String getUserNote() {
        return userNote;
    }

    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public Byte[] getImage() {
        return image;
    }

    @Override
    public void setUserId(String userId) {
        this.userId=userId;
    }

    @Override
    public void setUsername(String username) {
        this.username=username;
    }

    @Override
    public void setPassword(String password) {
        this.password=password;
    }

    @Override
    public void setEmail(String email) {
        this.email=email;
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber=phoneNumber;
    }

    @Override
    public void setUserNote(String userNote) {
        this.userNote=userNote;
    }

    @Override
    public void setFullName(String fullName) {
        this.fullName=fullName;
    }

    @Override
    public void setImage(Byte[] image) {
        this.image=image;
    }

    @Override
    public void Login() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
