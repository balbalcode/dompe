/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

/**
 *
 * @author Widyaranti
 */
public class Income implements models.Model_Income{
    private String incomeId;
    private String userId;
    private String incomeDate;
    private int incomeValue;
    private String incomeDesc;

    @Override
    public String getIncomeId() {
        return incomeId;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getIncomeDate() {
        return incomeDate;
    }

    @Override
    public int getIncomeValue() {
        return incomeValue;
    }

    @Override
    public String getIncomeDesc() {
        return incomeDesc;
    }

    @Override
    public void setIncomeId(String incomeId) {
        this.incomeId=incomeId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId=userId;
    }

    @Override
    public void setIncomeDate(String incomeDate) {
        this.incomeDate=incomeDate;
    }

    @Override
    public void setIncomeValue(int incomeValue) {
        this.incomeValue=incomeValue;
    }

    @Override
    public void setIncomeDesc(String incomeDesc) {
        this.incomeDesc=incomeDesc;
    }
    
    
}
