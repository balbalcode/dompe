/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import config.Database;
import java.sql.Connection;

/**
 *
 * @author Widyaranti
 */
public class Category implements models.Model_Category{
    private int categoryId;
    private int userId;
    private String categoryName;
    private String categoryDesc;
    
    private Connection conn=null;
    
    public Category(){
        conn=Database.getConnection();
    }

    @Override
    public int getCategoryId() {
        return categoryId;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public String geCategoryDesc() {
        return categoryDesc;
    }

    @Override
    public void setCategoryId(int categoryID) {
        this.categoryId=categoryID;
    }

    @Override
    public void setUserId(int userId) {
        this.userId=userId;
    }

    @Override
    public void setCategoryName(String categoryName) {
        this.categoryName=categoryName;
    }

    @Override
    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc=categoryDesc;
    }

    @Override
    public void saveCategory() {
        String sql = "";
    }

    @Override
    public void deleteCategory() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCategory() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
