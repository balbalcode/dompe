/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

/**
 *
 * @author Widyaranti
 */
public class Outcome implements models.Model_Outcome{
    private String outcomeId;
    private String userId;
    private String outcomeDate;
    private int outcomeValue;
    private String categoryId;
    private String outcomeDesc;

    @Override
    public String getOutcomeId() {
        return outcomeId;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getOutcomeDate() {
        return outcomeDate;
    }

    @Override
    public int getOutcomeValue() {
        return outcomeValue;
    }

    @Override
    public String getOutcomeCategoryId() {
        return categoryId;
    }

    @Override
    public String getOutcomeDesc() {
        return outcomeDesc;
    }

    @Override
    public void setOutcomeId(String outcomeId) {
        this.outcomeId=outcomeId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId=userId;
    }

    @Override
    public void setOutcomeDate(String outcomeDate) {
        this.outcomeDate=outcomeDate;
    }

    @Override
    public void setOutcomeValue(int outcomeValue) {
        this.outcomeValue=outcomeValue;
    }

    @Override
    public void setOutcomeCategoryId(String categoryId) {
        this.categoryId=categoryId;
    }

    @Override
    public void setOutcomeDesc(String outcomeDesc) {
        this.outcomeDesc=outcomeDesc;
    }
    
    
}
